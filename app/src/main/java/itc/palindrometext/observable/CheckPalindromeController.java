package itc.palindrometext.observable;

import android.os.Handler;
import android.os.Looper;

import java.util.HashSet;

import itc.palindrometext.model.Palindrome;

public class CheckPalindromeController {
    private HashSet<CheckPalindromeListener> mPalindromeListeners;
    private static CheckPalindromeController sCheckPalindromeController;

    private CheckPalindromeController() {
        mPalindromeListeners = new HashSet<>();
    }

    public static CheckPalindromeController getInstance() {
        if (sCheckPalindromeController == null) {
            sCheckPalindromeController = new CheckPalindromeController();
        }
        return sCheckPalindromeController;
    }

    public void addCheckPalindromeLister(CheckPalindromeListener checkPalindromeListener) {
        mPalindromeListeners.add(checkPalindromeListener);
    }

    public void removeCheckPalindromeListener(CheckPalindromeListener checkPalindromeListener) {
        mPalindromeListeners.remove(checkPalindromeListener);
    }

    public synchronized void notifyCheckPalindromeSuccess(Palindrome palindrome) {
        for (CheckPalindromeListener listener : mPalindromeListeners) {
            listener.notifyCheckPalindromeSuccess(palindrome);
        }
    }

    public void checkPalindromeText(final String inputText) {
        final Palindrome result = new Palindrome();
        if (inputText == null || inputText.isEmpty()) {
            result.setIsPalindrome(false);
            result.setText("Empty string");
            notifyCheckPalindromeSuccess(result);
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                StringBuilder alphanumericBuilder = new StringBuilder();
                for (char c : inputText.toCharArray()) {
                    if (Character.isLetter(c) || Character.isDigit(c)) {
                        alphanumericBuilder.append(c);
                    }
                }
                String alphanumericText = alphanumericBuilder.toString();
                String reserveInputText = alphanumericBuilder.reverse().toString();
                if (alphanumericBuilder.length() == 0) {
                    result.setIsPalindrome(false);
                } else {
                    result.setIsPalindrome(alphanumericText.equalsIgnoreCase(reserveInputText));
                }
                result.setText(inputText);
                Handler mainThreadHandler = new Handler(Looper.getMainLooper());
                mainThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        notifyCheckPalindromeSuccess(result);
                    }
                });
            }
        }).start();
    }
}
