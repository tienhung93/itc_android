package itc.palindrometext.observable;

import itc.palindrometext.model.Palindrome;

public interface CheckPalindromeListener {

    void notifyCheckPalindromeSuccess(Palindrome palindrome);
}
