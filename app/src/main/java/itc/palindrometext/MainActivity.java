package itc.palindrometext;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

import itc.palindrometext.fragment.InputFragment;
import itc.palindrometext.fragment.ResultFragment;

public class MainActivity extends Activity {

    private InputFragment mInputFragment;
    private ResultFragment mResultFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            return;
        }
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        mInputFragment = InputFragment.newInstance();
        mResultFragment = ResultFragment.newInstance();
        fragmentTransaction.replace(R.id.input_fragment_place, mInputFragment);
        fragmentTransaction.replace(R.id.result_fragment_place, mResultFragment);
        fragmentTransaction.commit();
    }
}
