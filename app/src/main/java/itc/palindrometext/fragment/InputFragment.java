package itc.palindrometext.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import itc.palindrometext.R;
import itc.palindrometext.observable.CheckPalindromeController;

public class InputFragment extends Fragment implements View.OnClickListener {

    private EditText mEdtInputName;

    public InputFragment() {
    }

    public static InputFragment newInstance() {
        return new InputFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.input_fragment, container, false);
        mEdtInputName = (EditText) view.findViewById(R.id.edt_input_name);
        view.findViewById(R.id.btn_submit).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                if (mEdtInputName != null) {
                    CheckPalindromeController.getInstance().
                            checkPalindromeText(mEdtInputName.getText().toString());
                    mEdtInputName.setText("");
                }
                break;
        }

    }
}
