package itc.palindrometext.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import itc.palindrometext.R;
import itc.palindrometext.model.Palindrome;
import itc.palindrometext.observable.CheckPalindromeController;
import itc.palindrometext.observable.CheckPalindromeListener;
import itc.palindrometext.settings.Constant;

public class ResultFragment extends Fragment implements CheckPalindromeListener {

    private static final String KEY_RESULTS = "results";
    private ArrayAdapter<String> mResultAdapter;
    private ArrayList<String> mListResult;
    private ListView mLvResults;

    public ResultFragment() {
    }

    public static ResultFragment newInstance() {
        return new ResultFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mListResult = savedInstanceState.getStringArrayList(KEY_RESULTS);
        }
        if (mListResult == null) {
            mListResult = new ArrayList<>();
        }
        mResultAdapter = new ArrayAdapter<>(
                getActivity(), android.R.layout.simple_list_item_1, mListResult);
        mLvResults.setAdapter(mResultAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.result_fragment, container, false);
        mLvResults = (ListView) view.findViewById(R.id.list_result);
        CheckPalindromeController.getInstance().addCheckPalindromeLister(this);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(KEY_RESULTS, mListResult);
    }


    @Override
    public void notifyCheckPalindromeSuccess(Palindrome palindrome) {
        if (getActivity() == null) {
            return;
        }
        mListResult.add(palindrome.toString(getActivity()));
        if (mListResult.size() > Constant.MAX_ITEM) {
            mListResult.remove(0);
        }
        mResultAdapter.notifyDataSetChanged();
    }
}
