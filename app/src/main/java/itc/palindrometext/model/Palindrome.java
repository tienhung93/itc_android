package itc.palindrometext.model;

import android.content.Context;

import itc.palindrometext.R;

public class Palindrome {
    private boolean mIsPalindrome;
    private String mText;

    public void setIsPalindrome(boolean isPalindrome) {
        this.mIsPalindrome = isPalindrome;
    }

    public void setText(String mText) {
        this.mText = mText;
    }

    public String toString(Context context) {
        int stringId = mIsPalindrome ? R.string.is_palindrome : R.string.not_palindrome;
        return context.getString(stringId, mText);
    }
}
